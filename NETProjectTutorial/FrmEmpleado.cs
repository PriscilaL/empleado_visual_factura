﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmEmpleado : Form
    {

        private DataTable tblEmpleado;
        private DataSet dsEmpleado;
        private BindingSource bsEmpleado;

        
        public FrmEmpleado()
        {
            InitializeComponent();
            bsEmpleado = new BindingSource();
            comboBox1.DataSource = Enum.GetValues(typeof(entities.Empleado.Sexo));
        }
        public DataTable TblEmpleados { set { tblEmpleado = value; } }
        public DataSet DsEmpleado { set { dsEmpleado = value; } }

        
    }
}
