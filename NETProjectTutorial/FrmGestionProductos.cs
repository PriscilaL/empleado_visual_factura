﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionProductos : Form
    {
        private DataSet dsProductos;
        private BindingSource bsProductos;

        public DataSet DsProductos { set { dsProductos = value; } }

        public FrmGestionProductos()
        {
            InitializeComponent();
            bsProductos = new BindingSource();
        }

   
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsProductos.Filter = string.Format("Sku like '*{0}*' or Nombre like '*{0}*' or Descripcion like '*{0}*' ", textBox1.Text);
               
            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmProducto fp = new FrmProducto();
            fp.TblProductos = dsProductos.Tables["Producto"];
            fp.DsProducto = dsProductos;
            fp.ShowDialog();
        }

        private void FrmGestionProductos_Load(object sender, EventArgs e)
        {
            bsProductos.DataSource = dsProductos;
            bsProductos.DataMember = dsProductos.Tables["Producto"].TableName;
            dataGridView1.DataSource = bsProductos;
            dataGridView1.AutoGenerateColumns = true;

        }
    }
}
