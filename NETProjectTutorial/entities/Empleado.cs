﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Empleado
    {
        private int id;
        private string ced;
        private string inss;
        private string nombre;
        private string apellido;
        private Sexo sexo;
        private string direccion;
        private string tel;
        private string cel;
        private double salario;


        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Ced
        {
            get
            {
                return ced;
            }

            set
            {
                ced = value;
            }
        }

        public string Inss
        {
            get
            {
                return inss;
            }

            set
            {
                inss = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string Apellido
        {
            get
            {
                return apellido;
            }

            set
            {
                apellido = value;
            }
        }

        

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public string Tel
        {
            get
            {
                return tel;
            }

            set
            {
                tel = value;
            }
        }

        public string Cel
        {
            get
            {
                return cel;
            }

            set
            {
                cel = value;
            }
        }

        public double Salario
        {
            get
            {
                return salario;
            }

            set
            {
                salario = value;
            }
        }

        internal Sexo Sexo1
        {
            get
            {
                return sexo;
            }

            set
            {
                sexo = value;
            }
        }

        

        public Empleado(Sexo sexo)
        {
            this.sexo = sexo;
        }

        public Empleado(int id, string ced, string inss, string nombre, string apellido, Sexo sexo, string direccion, string tel, string cel, double salario)
        {
            this.id = id;
            this.ced = ced;
            this.inss = inss;
            this.nombre = nombre;
            this.apellido = apellido;
            this.sexo = sexo;
            this.direccion = direccion;
            this.tel = tel;
            this.cel = cel;
            this.salario = salario;
        }

        public enum Sexo {
             Femenino,Masculino
        }

        public override string ToString()
        {
            return Ced + "" + Nombre + "" + Apellido;
        }
    }
}
